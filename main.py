# coding:utf8

'''
NPC 名字汉化
'''

import random

from peewee import (
    SqliteDatabase,
    Model,
    PrimaryKeyField,
    IntegerField,
    CharField,
)

db = SqliteDatabase('mir.sqlite')


class BaseModel(Model):
    class Meta:
        database = db


class NPC(BaseModel):
    _id = PrimaryKeyField(column_name='id')
    map_id = IntegerField()
    file_name = CharField()
    name = CharField()
    chinese_name = CharField()

    class Meta:
        db_table = 'npc'


class Item(BaseModel):
    _id = PrimaryKeyField(column_name='id')
    name = CharField()
    typ = IntegerField(column_name='type')

    class Meta:
        db_table = 'item'


chinese_name_map = {
    'Administrator': '管理员',
    'Prison': '监狱',
    'SubjugationLead': '战争使者',
    'Book': '书贩',
    'HighAssassin': '高阶刺客',
    'VulnerableSon': 'VulnerableSon',
    'Challange': '挑战',
    'Warehouse': '客栈',
    'MirGuide': '指南',
    'Mysterious': '神秘商贩',
    'Accessory': '罪犯',
    'OldFisherman': '渔夫',
    'Sir': '平民',
    'BorderVillage': '边境村',
    'MudWall': '土墙',
    'Librarian': '图书馆馆长',
    'Solider': '士兵',
    'PetMaster': '宠物管理员',
    'LeadTrainer': 'LeadTrainer',
    'FishMonger': '鱼贩子',
    'HighPriest': 'HighPriest',
    'SquadLeader': 'SquadLeader',
    'Assistant': '助理',
    'Drapery': 'Drapery',
    'ArchMage': 'ArchMage',
    'Lottery': 'Lottery',
    'StableGirl': 'StableGirl',
    'BichonSoldier': '比奇卫兵',
    'Grocery': 'Grocery',
    'Traveller': 'Traveller',
    'Premium': 'Premium',
    'Sailor': 'Sailor',
    'Emperor': 'Emperor',
    'MasterMK': 'MasterMK',
    'InnKeeper': '旅店老板',
    'TrustMerchant': 'TrustMerchant',
    'GM': 'GM',
    'LeftEscort': 'LeftEscort',
    'Teleport': '传送员',
    'RightEscort': 'RightEscort',
    'General': '将军',
    'Captain': 'Captain',
    'Specialist': 'Specialist',
    'TheWatcher': 'TheWatcher',
    'Transport': '传送员',
    'BichonInspector': 'BichonInspector',
    'Inspector': 'Inspector',
    'Commander': 'Commander',
    'Blacksmith': '铁匠',
    'WiseFisherman': 'WiseFisherman',
    'Merchant': '商人',
    'WickedTrader': 'WickedTrader',
    'Investigator': 'Investigator',
    'VillageChief': 'VillageChief',
    'GTMerchant': 'GTMerchant',
    'CraftsLady': 'CraftsLady',
    'SubjagationManager': 'SubjagationManager',
    'CraftingVillage': 'CraftingVillage',
    'Excavenger': 'Excavenger',
    'MongchonDelegate': 'MongchonDelegate',
    'CaveGuide': '洞穴指南',
    'TravellingMerchant': '流浪商人',
    'Master': 'Master',
    'Examiner': 'Examiner',
    'MaterialDealer': 'MaterialDealer',
    'Storage': '仓库',
    'Alchemist': 'Alchemist',
    'Protector': 'Protector',
    'BichonWall': 'BichonWall',
    'MongchonScout': 'MongchonScout',
    'PotionShop': '药剂商店',
}


def _translate(english_name):
    left, right = english_name.split('_')
    # print(left, right)
    if not left:
        return english_name
    left = chinese_name_map[left]
    if right in ['Board', 'Guard', 'Portal']:
        right = {'Board': '告示', 'Guard': '守卫', 'Portal': '入口'}.get(right)
    else:
        r1 = ['赵', '钱', '孙', '李', '周', '吴', '郑', '王', '冯', '陈', '褚', '卫', '蒋', '沈', '韩', '杨', '朱', '秦', '尤', '许',
              '何', '吕', '施', '张', '孔', '曹', '严', '华', '金', '魏', '陶', '姜', '戚', '谢', '邹', '喻', '柏', '水', '窦', '章',
              '云', '苏', '潘', '葛', '奚', '范', '彭', '郎', '鲁', '韦', '昌', '马', '苗', '凤', '花', '方', '俞', '任', '袁', '柳',
              '酆', '鲍', '史', '唐', '费', '廉', '岑', '薛', '雷', '贺', '倪', '汤', '滕', '殷', '罗', '毕', '郝', '邬', '安', '常',
              '乐', '于', '时', '傅', '皮', '卞', '齐', '康', '伍', '余', '元', '卜', '顾', '孟', '平', '黄', '和', '穆', '萧', '尹',
              '姚', '邵', '湛', '汪', '祁', '毛', '禹', '狄', '米', '贝', '明', '臧', '计', '伏', '成', '戴', '谈', '宋', '茅', '庞',
              '熊', '纪', '舒', '屈', '项', '祝', '董', '梁', '杜', '阮', '蓝', '闵', '席', '季', '麻', '强', '贾', '路', '娄', '危',
              '江', '童', '颜', '郭', '梅', '盛', '林', '刁', '钟', '徐', '邱', '骆', '高', '夏', '蔡', '田', '樊', '胡', '凌', '霍',
              '虞', '万', '支', '柯', '昝', '管', '卢', '莫', '经', '房', '裘', '缪', '干', '解', '应', '宗', '丁', '宣', '贲', '邓',
              '万俟', '司马', '上官', '欧阳', '夏侯', '诸葛',
              '闻人', '东方', '赫连', '皇甫', '尉迟', '公羊', '澹台', '公冶', '宗政', '濮阳',
              '淳于', '单于', '太叔', '申屠', '公孙', '仲孙', '轩辕', '令狐', '钟离', '宇文']
        r2 = ['妙梦', '雪容', '静柏', '易云', '白风', '曼安', '沛亦', '映寒', '访梅', '映柏', '小雪', '安亦', '晓易', '寒丝',
              '丹琴', '听筠', '之梦', '妙风', '碧芙', '友枫', '曼萍', '傲阳', '乐儿', '雅风', '思松', '依岚', '鄂绿', '荷虞', '真南',
              '保艳', '娜娜', '鹤荣', '家兴', '瑞丽', '德丽', '文杰', '永香', '永真', '春东', '艳蕾', '利彬', '建伟', '苗苗', '晓莉', '松伟', '可慧', '思涵', '东方', '继宽', '付楠', '诗诗', '志鸽', '春生', '世梅', '慧利', '忠娟', '振琪', '少杰', '宁宁', '彦峰', '金鹏', '成立', '治霞', '清梅', '英瑞', '爱涛', '红岩', '江潜', '天帅', '刚春', '明',
              '俊锡', '东辰', '浩宇', '芯依', '宇航', '逸龙', '一哲', '梓睿', '伊可', '宁馨', '晨曦', '恒硕', '自乐', '统乐', '致远', '悦辰', '静依', '馨然', '玉灿', '新云', '柯欣', '玉萱', '钰莹', '诗雯', '柯豫', '悦弘', '楚萓', '晏宇', '东成', '博文', '豫豪', '世杰', '炳錦', '雨诺', '一诺', '圣恩', '天朝', '树果', '玉宁', '柯豪', '娅廷', '浩然', '沐语', '树泽', '玉研', '世暄', '统勋', '子尧', '斐然', '啸喆', '欣怡', '树辰', '雨泽', '司翰', '旭来', '子伯', '士航', '炳硕', '康帅', '恒鑫', '一鸣', '东耀', '圣贤', '朝曦', '子辰', '亦凡', '冠羽', '玉戈', '嘉兴', '冰可', '珂簪', '鑫钰', '洪滨',
              '媚', '婷', '淑', '婵', '风', '水', '嫣', '黛', '女', '香', '君', '娟', '鹃', '卯', '甲', '冬', '仪', '妆', '珍', '俏', '画', '静', '丑', '酉', '衣', '鸾', '培', '亥', '倩', '子', '丙', '庚', '胭', '书', '璧', '妍', '申', '戊', '辰', '霜', '午', '环', '云', '月', '雁', '雨', '癸', '裳', '琴', '乙', '绸', '己', '钗', '辛', '琬', '溪', '丁', '未', '戌', '烟', '婉', '雪', '凰',
              ]
        right = random.choice(r1) + random.choice(r2)
    return left + '_' + right


def npc_chinese_name():
    npcs = NPC.select()
    for npc in npcs:
        name = npc.name
        npc.chinese_name = name
        if '_' in name:
            npc.chinese_name = _translate(name)
        npc.save(force_insert=False, only=['chinese_name'])


def change_item_type():
    item = Item.get(Item.name == '野鹿肉')
    item.typ = 15
    item.save(force_insert=False, only=['type'])


def main():
    # 给 npc 取个中文名
    # npc_chinese_name()

    # 修改物品类型
    change_item_type()


if __name__ == "__main__":
    main()
